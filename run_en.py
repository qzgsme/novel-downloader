#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup
import logging
import json
from concurrent.futures import ThreadPoolExecutor

CHAPTER_LIST_URL = "https://www.webnovel.com/apiajax/chapter/GetChapterList?_csrfToken=bEO9JhC3i9H9PAohrWimKoJIVIY7B5H3L5FPmuq6&bookId=7176992105000305&_=1499351707383"
BASE_URL = "https://www.webnovel.com/book/7176992105000305/"
CHAPTER_INDEX = []


def fetch_web(url, res_type="text"):
    r = requests.get(url)
    r.encoding = 'utf-8'
    if r.status_code != 200:
        logging.error("Cannot get {}".format(url))
        return None

    if res_type == "json":
        return r.json()
    else:
        return r.text


def get_chapter_detail(chapter):
    logging.info("Process chapter {}".format(chapter['cid']))
    url = "{}{}".format(BASE_URL, chapter['real_id'])
    fetch_content = fetch_web(url)

    if fetch_content is None:
        logging.error("Cannot get chapter url {}".format(url))
        return

    content = parse_chapter_detail(fetch_content)

    # generate chapter json file
    file_data = {
        'cid': chapter['cid'],
        'title': chapter['title'],
        'content': content
    }

    file_name = "chapter_{}".format(chapter['cid'])
    write_json_file(file_name, file_data)


def parse_chapter_detail(html_doc):
    soup = BeautifulSoup(html_doc, 'lxml')
    page_content_list = soup.select(".cha-content > p")

    # purify chapter detail
    chapter_content = []
    for el in page_content_list:
        el_str = purify_text(el)
        if el_str:
            chapter_content.append(el_str)

    page_content = "".join(chapter_content)
    page_content += "<p>---------------------</p>"
    page_content += "<p>Quan Zhi Gao Shou ( The King's Avatar ) novel powered by QZGS.ME</p>"

    return page_content


def purify_text(el):
    el_text = el.get_text()
    purify_text = str(el_text).strip()
    if purify_text:
        return "<p>{}</p>".format(purify_text)
    else:
        return None


def write_json_file(file_name, file_data):
    file_path = "{}/{}.json".format('./data', file_name)
    with open(file_path, "w", encoding='utf8') as f:
        json.dump(file_data, f, ensure_ascii=False)


def generate_chapter_list():
    chapter_list = fetch_web(CHAPTER_LIST_URL, "json")
    if chapter_list is None:
        logging.error("Cannot get chapter list!")
        exit(1)

    if chapter_list['code'] != 0:
        logging.error("Parse chapter list error!")
        exit(1)

    chapter_list_res = []
    for el in chapter_list['data']['chapterItems']:
        chapter_list_res.append({
            'cid': el['chapterIndex'],
            'title': el['chapterName'],
            'real_id': el['chapterId']
        })

    chapter_index_sorted = sorted(chapter_list_res, key=lambda x: x['cid'])
    logging.info("Generate chapter index done!")

    chapter_list_file = [{
        'cid': el['cid'],
        'title': el['title']
    } for el in chapter_index_sorted]

    write_json_file("chapter_list_all", chapter_list_file)

    return chapter_index_sorted


def run():
    chapter_list = generate_chapter_list()

    if chapter_list is None:
        exit(1)

    # chapter_list = chapter_list[686:]

    with ThreadPoolExecutor(max_workers=5) as executor:
        for el in chapter_list:
            executor.submit(get_chapter_detail, el)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    run()
