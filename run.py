#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup
import logging
import json
from concurrent.futures import ThreadPoolExecutor
from threading import Lock
from opencc import OpenCC

BASE_URL = "http://m.shushu8.com/quanzhigaoshou/"
CHAPTER_INDEX = []

CHAPTER_FIX = {
    149: {'url': '145_2', 'title': '第一百四十九章 大竞价'},
    288: {'url': '2808', 'title': '第二八十八章 超级强力M'},
    466: {'url': '4606', 'title': '第四百六十六章 非常规的手段'},
    534: {'url': '544', 'title': '第五百三十四章 毒蝇缠身'},
    535: {'url': '545', 'title': '第五百三十五章 凶猛的螳螂'},
    539: {'url': '139_2', 'title': '第五百三十九章 猎杀结束'},
    544: {'url': '544_2', 'title': '第五百四十四章 后续还有三'},
    545: {'url': '545_2', 'title': '第五百四十五章 营救'},
    558: {'url': '557_2', 'title': '第五百五十八章 试探'},
    591: {'url': '590_2', 'title': '第五百九十一章 公会苦力'},
    1041: {'url': '1040_2', 'title': '第一千零四十一章 熟悉的一幕又一幕'},
    1149: {'url': '1150', 'title': '第一千一百四十九章 气功和刺客（下）'},
    1150: {'url': '1150_2', 'title': '第一千一百五十章 花谢花开'},
    1159: {'url': '1189', 'title': '第一千一百五十九章 不该出现的'},
    1160: {'url': '1159', 'title': '第一千一百六十章 湍急的断河'},
    1189: {'url': '1189_2', 'title': '第一千一百八十九章 鬼连环'},
    1310: {'url': '3010', 'title': '第一千三百一十章 有得改进'},
    1489: {'url': '1089_2', 'title': '第一千四百八十九章 疯狂的百花'},
    1490: {'url': '1390_2', 'title': '第一千四百九十章 逃跑不是目的'},
    1600: {'url': '1560_2', 'title': '第一千六百章 操作强吃'},
    1631: {'url': '1031_2', 'title': '第一千六百三十一章 建立胜机的坚持'},
    1725: {'url': '70205', 'title': '第一千七十二十五章 他们是冠军'},
}

openCC = OpenCC()
openCC.set_conversion('s2twp')


def fetch_web(url):
    r = requests.get(url)
    r.encoding = 'gbk'
    if r.status_code != 200:
        logging.error("Cannot get {}".format(BASE_URL))
        return None

    return r.text


def get_chapter_detail(cid, lock):
    logging.info("Process chapter {}".format(cid))

    # need to fix chapter url
    if cid in CHAPTER_FIX:
        new_url = CHAPTER_FIX[cid]['url']
        url = "{}{}.html".format(BASE_URL, new_url)
    else:
        url = "{}{}.html".format(BASE_URL, cid)

    fetch_content = fetch_web(url)

    if fetch_content is None:
        logging.error("Cannot get url {}".format(url))
        return

    title, content = parse_chapter_detail(fetch_content, cid)

    # generate CHAPTER_INDEX
    with lock:
        CHAPTER_INDEX.append({
            'cid': cid,
            'title': title
        })

    # generate chapter json file
    file_data = {
        'cid': cid,
        'title': title,
        'content': content
    }

    file_name = "chapter_{}".format(cid)
    write_json_file(file_name, file_data)


def parse_chapter_detail(html_doc, cid):
    soup = BeautifulSoup(html_doc, 'html.parser')
    page_title = soup.select("#nr_title")[0].contents[0]
    page_content_list = soup.select("#content")[0].contents

    # purify chapter detail
    chapter_content = []
    for el in page_content_list:
        el_str = purify_text(el)

        if el_str:
            chapter_content.append(el_str)

    page_content = "<p>" + "".join(chapter_content) + "</p>"
    page_content += "<p>---------------------</p>"
    page_content += "<p>全職高手動漫，漫畫，小說，盡在 QZGS.ME</p>"

    # need to fix title
    if cid in CHAPTER_FIX:
        page_title = CHAPTER_FIX[cid]['title']

    page_title = openCC.convert(page_title)
    return page_title, page_content


def purify_text(words):
    words = str(words).strip()

    # remove m.shushu8.com
    if "m.shushu8.com" in words:
        return None

    # replace <br/> to </p><p>
    if words == "<br/>":
        return "</p><p>"

    # replace 艹 “ ”
    words = words.replace("艹", "操")
    words = words.replace("“", "「")
    words = words.replace("”", "」")
    words = words.replace("<div class=\"readmidad\"><script>read_adout('read_middle');</script></div>", "")

    # trans to Traditional Chinese
    return openCC.convert(words)


def write_json_file(file_name, file_data):
    file_path = "{}/{}.json".format('./data', file_name)
    with open(file_path, "w", encoding='utf8') as f:
        json.dump(file_data, f, ensure_ascii=False)


def generate_chapter_list():
    chapter_index_sorted = sorted(CHAPTER_INDEX, key=lambda x: x['cid'])
    write_json_file("chapter_list_all", chapter_index_sorted)
    logging.info("Generate chapter index done!")


def run():
    lock = Lock()
    with ThreadPoolExecutor(max_workers=10) as executor:
        for el in range(1, 1729):
            executor.submit(get_chapter_detail, el, lock)

    generate_chapter_list()


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    run()
